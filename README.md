# Ansi

"ansi-escape-sequences" is a Simple tool to use ansi escape sequences

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install [ansi-escape-sequences](https://pypi.org/project/ansi-escape-sequences/).

```bash
pip install ansi-escape-sequences
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
