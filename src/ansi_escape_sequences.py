#!/usr/bin/python3
# -*- coding: utf-8 -*-
# https://en.wikipedia.org/wiki/ANSI_escape_code

from math import floor
from os import get_terminal_size

ESC = '\033'

SS2 = ESC + 'N'
SS3 = ESC + 'O'
DCS = ESC + 'P'
CSI = ESC + '['
ST = ESC + '\\'
OSC = ESC + ']'
SOS = ESC + 'X'
PM = ESC + '^'
APC = ESC + '_'

BEL = '\007'


def center(text: str) -> str:
    for i in range(floor(get_terminal_size().columns/2) - len(text)):
        text = " " + text
    return text


def set_title(title: str) -> str:
    """
    Xterm allows the window title to be set by `ESC ]0;this is the window title BEL`.
    """
    return OSC + '2;' + title + BEL


class Cursor(object):
    @staticmethod
    def Up(n: int = 1) -> str:
        """Moves the cursor n (default `1`) cells in the given direction. If the cursor is already at the edge of the screen, this has no effect."""
        return CSI + str(n) + 'A'

    @staticmethod
    def Down(n: int = 1) -> str:
        """Moves the cursor n (default `1`) cells in the given direction. If the cursor is already at the edge of the screen, this has no effect."""
        return CSI + str(n) + 'B'

    @staticmethod
    def Forward(n: int = 1) -> str:
        """Moves the cursor n (default `1`) cells in the given direction. If the cursor is already at the edge of the screen, this has no effect."""
        return CSI + str(n) + 'C'

    @staticmethod
    def Back(n: int = 1) -> str:
        """Moves the cursor n (default `1`) cells in the given direction. If the cursor is already at the edge of the screen, this has no effect."""
        return CSI + str(n) + 'D'

    @staticmethod
    def NextLine(n: int = 1) -> str:
        """
        Moves cursor to beginning of the line n (default 1) lines down.
        (not [ANSI.SYS](https://en.wikipedia.org/wiki/ANSI.SYS))
        """
        return CSI + str(n) + 'E'

    @staticmethod
    def PreviousLine(n: int = 1) -> str:
        """
        Moves cursor to beginning of the line n (default 1) lines up.
        (not [ANSI.SYS](https://en.wikipedia.org/wiki/ANSI.SYS))
        """
        return CSI + str(n) + 'F'

    @staticmethod
    def HorizontalAbsolute(n: int = 1) -> str:
        """
        Moves the cursor to column n (default 1).
        (not [ANSI.SYS](https://en.wikipedia.org/wiki/ANSI.SYS))
        """
        return CSI + str(n) + 'G'

    @staticmethod
    def Position(x: int = 1, y: int = 1) -> str:
        """
        Moves the cursor to row n, column m.
        The values are 1-based, and default to `1` (top left corner) if omitted.
        A sequence such as `CSI ;5H` is a synonym for `CSI 1;5H` as well as `CSI 17;H` is the same as `CSI 17H` and `CSI 17;1H`
        """
        return CSI + str(y) + ';' + str(x) + 'H'

    @staticmethod
    def HorizontalVerticalPosition(x: int = 1, y: int = 1) -> str:
        """
        Same as CUP, but counts as a format effector function (like [CR](https://en.wikipedia.org/wiki/Carriage_return) or [LF](https://en.wikipedia.org/wiki/Line_feed)) rather than an editor function (like CUD or CNL).
        This can lead to different handling in certain terminal modes.
        """
        return CSI + str(x) + ';' + str(y) + 'f'

    @staticmethod
    def SaveCurrentPosition() -> str:
        """
        Saves the cursor position/state in SCO console mode.
        In vertical split screen mode, instead used to set (as `CSI n ; n s`) or reset left and right margins.
        """
        return CSI + 's'

    @staticmethod
    def RestoreSavedPosition() -> str:
        """Restores the cursor position/state in SCO console mode."""
        return CSI + 'u'

    @staticmethod
    def Show() -> str:
        """Shows the cursor, from the [VT220](https://en.wikipedia.org/wiki/VT220)."""
        return CSI + "?25h"

    @staticmethod
    def Hide() -> str:
        """Hides the cursor."""
        return CSI + "?25l"


class Erase(object):
    @staticmethod
    def Display(mode: int = 2) -> str:
        """
        Clears part of the screen.
        If n is `0` (or missing), clear from cursor to end of screen.
        If n is `1`, clear from cursor to beginning of the screen.
        If n is `2`, clear entire screen (and moves cursor to upper left on DOS ANSI.SYS).
        If n is `3`, clear entire screen and delete all lines saved in the scrollback buffer
        (this feature was added for [xterm](https://en.wikipedia.org/wiki/Xterm) and is supported by other terminal applications). 
        """
        return CSI + str(mode) + 'J'

    @staticmethod
    def Line(mode: int = 2) -> str:
        """
        Erases part of the line.
        If n is `0` (or missing), clear from cursor to the end of the line.
        If n is `1`, clear from cursor to beginning of the line.
        If n is `2`, clear entire line. Cursor position does not change.
        """
        return CSI + str(mode) + 'K'


class Scroll(object):
    @staticmethod
    def Up(n: int = 1) -> str:
        """
        Scroll whole page up by n (default `1`) lines. New lines are added at the bottom.
        (not [ANSI.SYS](https://en.wikipedia.org/wiki/ANSI.SYS))
        """
        return CSI + str(n) + 'S'

    @staticmethod
    def Down(n: int = 1) -> str:
        """
        Scroll whole page down by n (default `1`) lines. New lines are added at the top.
        (not [ANSI.SYS](https://en.wikipedia.org/wiki/ANSI.SYS))
        """
        return CSI + str(n) + 'T'


class AUXPort(object):
    @staticmethod
    def On() -> str:
        """Enable aux serial port usually for local serial printer"""
        return CSI + "5i"

    @staticmethod
    def Off() -> str:
        """Disable aux serial port usually for local serial printer"""
        return CSI + "4i"


class ScreenBuffer(object):
    @staticmethod
    def Enable() -> str:
        """Enable alternative screen buffer, from xterm"""
        return CSI + "?1049h"

    @staticmethod
    def Disable() -> str:
        """Disable alternative screen buffer, from xterm"""
        return CSI + "?1049l"


class Bracketed(object):
    @staticmethod
    def On() -> str:
        """
        Turn on bracketed paste mode.
        Text pasted into the terminal will be surrounded by `ESC [200~` and `ESC [201~`,
        and characters in it should not be treated as commands (for example in Vim). From xterm
        """
        return CSI + "?2004h"

    @staticmethod
    def Off() -> str:
        """
        Turn off bracketed paste mode.
        """
        return CSI + "?2004l"


def bit3_4(n: int = 0, m: int = 0) -> str:
    """
    [3-bit and 4-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit)
    """
    return CSI + str(n) + ';' + str(m) + 'm'


def bit8(n: int = 0) -> str:
    """
    [8-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit)
    """
    return ";5;" + str(n)


def foregroundbit8(n: int = 0) -> str:
    """
    [8-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit)
    """
    return CSI + str(38) + bit8(n) + 'm'


def backgroundbit8(n: int = 0) -> str:
    """
    [8-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit)
    """
    return CSI + str(48) + bit8(n) + 'm'


def bit24(r: int = 0, g: int = 0, b: int = 0) -> str:
    """
    [24-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit)
    """
    return ";2;" + str(r) + ';' + str(g) + ';' + str(b)


def foregroundbit24(r: int = 0, g: int = 0, b: int = 0) -> str:
    """
    [24-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit)
    """
    return CSI + str(38) + bit24(r, g, b) + 'm'


def backgroundbit24(r: int = 0, g: int = 0, b: int = 0) -> str:
    """
    [24-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit)
    """
    return CSI + str(48) + bit24(r, g, b) + 'm'


def reset() -> str:
    """All attributes off"""
    return CSI + 'm'


def add_CSI(n) -> str:
    return CSI + str(n) + 'm'


class Intensity(object):
    INCREASED = add_CSI(1)
    DECREASED = add_CSI(2)
    NORMAL = add_CSI(22)

    # aliases
    DIM = DECREASED
    BOLD = INCREASED
    RESET = NORMAL


class Italic(object):
    ON = add_CSI(3)
    OFF = add_CSI(23)


class Underline(object):
    ON = add_CSI(4)
    DOUBLY = add_CSI(21)
    OFF = add_CSI(24)
    DEFAULT_COLOR = add_CSI(59)


class Blink(object):
    SLOW = add_CSI(5)
    RAPID = add_CSI(6)
    NORMAL = add_CSI(25)


class Reverse(object):
    ON = add_CSI(7)
    OFF = add_CSI(27)


class Conceal(object):
    ON = add_CSI(8)
    NORMAL = add_CSI(28)


class CrossedOut(object):
    ON = add_CSI(9)
    OFF = add_CSI(29)


class Strikethrough(CrossedOut):
    pass


class Font(object):
    DEFAULT = add_CSI(10)
    FONT_1 = add_CSI(11)
    FONT_2 = add_CSI(12)
    FONT_3 = add_CSI(13)
    FONT_4 = add_CSI(14)
    FONT_5 = add_CSI(15)
    FONT_6 = add_CSI(16)
    FONT_7 = add_CSI(17)
    FONT_8 = add_CSI(18)
    FONT_9 = add_CSI(19)


class Fraktur(object):
    ON = add_CSI(20)
    OFF = add_CSI(23)


class ProportionalSpacing(object):
    ON = add_CSI(26)
    OFF = add_CSI(50)


class Encircled(object):
    ON = add_CSI(52)
    OFF = add_CSI(54)


class Framed(object):
    ON = add_CSI(51)
    OFF = add_CSI(54)


class Overlined(object):
    ON = add_CSI(53)
    OFF = add_CSI(55)


class Ideogram(object):
    UNDERLINE_OR_RIGHT_SIDE_LINE = add_CSI(60)
    DOUBLE_UNDERLINE_OR_DOUBLE_LINE_ON_THE_RIGHT_SIDE = add_CSI(61)
    OVERLINE_OR_LEFT_SIDE_LINE = add_CSI(62)
    DOUBLE_OVERLINE_OR_DOUBLE_LINE_ON_THE_LEFT_SIDE = add_CSI(63)
    STRESS_MARKING = add_CSI(64)
    OFF = add_CSI(65)


class Script(object):
    SUPER = add_CSI(73)
    SUB = add_CSI(74)


class Background(object):
    """
    [3-bit and 4-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit)
    """
    BLACK = add_CSI(40)
    RED = add_CSI(41)
    GREEN = add_CSI(42)
    YELLOW = add_CSI(43)
    BLUE = add_CSI(44)
    MAGENTA = add_CSI(45)
    CYAN = add_CSI(46)
    WHITE = add_CSI(47)

    BRIGHT_BLACK = add_CSI(100)
    BRIGHT_RED = add_CSI(101)
    BRIGHT_GREEN = add_CSI(102)
    BRIGHT_YELLOW = add_CSI(103)
    BRIGHT_BLUE = add_CSI(104)
    BRIGHT_MAGENTA = add_CSI(105)
    BRIGHT_CYAN = add_CSI(106)
    BRIGHT_WHITE = add_CSI(107)

    DEFAULT = add_CSI(49)

    # aliases
    RESET = DEFAULT


class Foreground(object):
    """
    [3-bit and 4-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit)
    """
    BLACK = add_CSI(30)
    RED = add_CSI(31)
    GREEN = add_CSI(32)
    YELLOW = add_CSI(33)
    BLUE = add_CSI(34)
    MAGENTA = add_CSI(35)
    CYAN = add_CSI(36)
    WHITE = add_CSI(37)

    BRIGHT_BLACK = add_CSI(90)
    BRIGHT_RED = add_CSI(91)
    BRIGHT_GREEN = add_CSI(92)
    BRIGHT_YELLOW = add_CSI(93)
    BRIGHT_BLUE = add_CSI(94)
    BRIGHT_MAGENTA = add_CSI(95)
    BRIGHT_CYAN = add_CSI(96)
    BRIGHT_WHITE = add_CSI(97)

    DEFAULT = add_CSI(39)

    # aliases
    RESET = DEFAULT


if __name__ == '__main__':

    print(
        Foreground.BLACK +
        Intensity.INCREASED +
        center("Ansi.py") +
        Intensity.NORMAL +
        Foreground.DEFAULT
    )

    print()

    print("Styles:")

    print(Intensity.INCREASED + "bold" + Intensity.NORMAL)
    print(Intensity.DECREASED + "dim" + Intensity.NORMAL)
    print(Italic.ON + "italic" + Italic.OFF)
    print(Underline.ON + "underline" + Underline.OFF)
    print(Blink.RAPID + "blink" + Blink.NORMAL)
    print(Strikethrough.ON + "strikethrough" + Strikethrough.OFF)
    print(Reverse.ON + "reverse" + Reverse.OFF)

    print()

    print("Colors:")

    print("       Foreground:")

    print("                  " + Background.WHITE + Foreground.BLACK +
          "BLACK" + Foreground.RESET + Background.RESET)
    print("                  " + Foreground.RED + "RED" + Foreground.RESET)
    print("                  " + Foreground.GREEN + "GREEN" + Foreground.RESET)
    print("                  " + Foreground.YELLOW + "YELLOW" + Foreground.RESET)
    print("                  " + Foreground.BLUE + "BLUE" + Foreground.RESET)
    print("                  " + Foreground.MAGENTA+"MAGENTA"+Foreground.RESET)
    print("                  " + Foreground.CYAN + "CYAN" + Foreground.RESET)
    print("                  " + Foreground.WHITE + "WHITE" + Foreground.RESET)

    print("       Background:")

    print("                  " + Background.BLACK + "BLACK" + Background.RESET)
    print("                  " + Background.RED + "RED" + Background.RESET)
    print("                  " + Background.GREEN + "GREEN" + Background.RESET)
    print("                  " + Background.YELLOW + "YELLOW" + Background.RESET)
    print("                  " + Background.BLUE + "BLUE" + Background.RESET)
    print("                  " + Background.MAGENTA+"MAGENTA"+Background.RESET)
    print("                  " + Background.CYAN + "CYAN" + Background.RESET)
    print("                  " + Foreground.BLACK + Background.WHITE +
          "WHITE" + Background.RESET + Foreground.RESET)
